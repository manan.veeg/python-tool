#import library statement important library tk,filedialog,ttk,pandas
import tkinter as tk
from tkinter import filedialog
from tkinter import ttk
import pandas as pd
#this list to use track-recored of opening file
Opean_file_path = [] #storing file location in String type
datgramoffileVariable = [] #Storing Dynamic Variable making in variablemaking that each variable map to dataframe
frametolist = [] # convert all dataframe label into list value
#######STEP_9###########
#Merge all excel into one single in which have same partnumber and Description start with CAP THICK can merge all to one qauntity
def csv_making():
    for i in range(len(datgramoffileVariable)):
        frametolist.extend(datgramoffileVariable[i].values.tolist())
    length = len(frametolist)
    print(frametolist)
    i = 0
    while i < length:
        j = i + 1
        while j < length:
            
            if(frametolist[i][0] == frametolist[j][0]):
                frametolist[i][5] = frametolist[i][5] +frametolist[j][5]
                frametolist.pop(j)
                length = len(frametolist)
                
            elif frametolist[i][2] == frametolist[j][2]:
                frametolist[i][5] = frametolist[i][5] + frametolist[j][5]
                frametolist.pop(j)
                length = len(frametolist)
            j = j + 1    
        i = i + 1
    
    df = pd.DataFrame(frametolist)
    print(df)
    df.to_excel('C:/Users/pd/Desktop/out.xlsx')
#######STEP_8###########
#geting file index that you want to maniuplate
def findindex(tar):
    #Searching Opean_file_path list if through iterate the loop not found element return -1 else return index.
    for i in range(len(Opean_file_path)):
        if Opean_file_path[i][1] == tar:
            return i
    return -1
#######STEP_7###########
#get Filename and qauntity
def getvalue(i): 
    #this send tuple if number enter from user qauntity field if not numeric value send -1
    if Opean_file_path[i][2].get().isnumeric():
        return Opean_file_path[i][1],Opean_file_path[i][2].get(),Opean_file_path[i][0]
    else:
        return -1
#######STEP_6###########
#perfom change Qauntity into final predefined dataframe coloumn of file
def compute():
    #we opean file path that stored in Opean_file_path we wanted to make back the Filename , file qauntity that you want to change and perfrom dataframe computes
    for i in range(len(Opean_file_path)):
        v = getvalue(i) # it return tuple of value.
        #tuple format : (1)file name,(2)get value of that input,(3)file path
        if v == -1:
            pass
        else:
            index = findindex(v[0])
            datgramoffileVariable[index].Quantity = datgramoffileVariable[index].Quantity * int(v[1])
    print("Compute part is Done 65% work is Done")  
    print(datgramoffileVariable[0])          
#######STEP_5###########
def ShowOpeanFile():
    col = 0
    ro = 0
    for i in range(len(Opean_file_path)):
        ro = ro + 1
        for j in range(len(Opean_file_path[i])):
            e = 0
            if j > 1:
                e = tk.Entry(window)
                e.grid(row=ro, column=col)
                Opean_file_path[i][j] = e
            else:
                e = tk.Entry(window, width=20, fg='blue')
                e.grid(row=ro,column=col)
                e.insert(tk.END,Opean_file_path[i][j])
                col = col + 1
        col = col - 2
    # print("ShowOpeanFile Done")
#######STEP_4###########
def variablemaking(size):#here passing value of opean-file-path that already In Opean_file_path list
    temp = []
    for number in range(size):
        temp.append(str(number)) 
        temp = (list(set(temp)))
    datgramoffileVariable.clear()
    for i in range(len(temp)):
        datgramoffileVariable.append(temp[i])
    # print("Variable making Done")
#######STEP_3###########
def Converxlsx():
    #make Another list according to opean file path that map each seprate Dataframe to One
    variablemaking(len(Opean_file_path)) #function call
    #Convert each open file path list of file into Dataframe. 
    for i in range(len(datgramoffileVariable)):
        datgramoffileVariable[i] = pd.read_excel(Opean_file_path[i][0])
    # print('Convert excel done')
#######STEP_2###########
#Open-file or select path saving into list and another FUnction calls
def browseFiles():
    filename = filedialog.askopenfilename(initialdir = "/",
                                          title = "Select a File",
                                          filetypes = [("Excel file","*.xlsx"),("Excel file", "*.xls")]
                                        )
    #If same File Open Again Showing error in Label and Not include in Opean_file_path list
    if(filename in Opean_file_path):
        pass
    else:
        #in this list first thing is (0)file location , (1)file name , (2)QauntityBox
        tmp = []
        tmp.append(filename)#fileName
        #processing part of distincit path location and file name and add file name to final list
        f=filename.split('/')
        tmp.append(f[-1])
        tmp.append('qauntitybox') #Qauntitybox for rendaring Qauntity box
        Opean_file_path.append(tmp)
        # print(tmp)
        Converxlsx()
        ShowOpeanFile()
    print("browseFile and convert file into dataframe Done 25% Done")
#######STEP_1###########
#UI
window = tk.Tk()
#file name path label making and attach to main window
label_for_fileNamepath = tk.Label(window ,text="File Name path",
                            width=20,height=3)
label_for_fileNamepath.grid(column=0,row=0)
#file name label  attach to main window
label_for_fileName = tk.Label(window ,text="File Name Here",
                            width=20,height=3)
label_for_fileName.grid(column=1,row=0)
#file quantity Field label to main window
label_for_qautity = tk.Label(window ,text="Enter the Qauntity",
                            width=20,height=3)
label_for_qautity.grid(column=2,row=0)
#exlpore file button and map with main window
button_explore = tk.Button(window,
                        text = "Browse Files here",
                        command=lambda :[browseFiles()] )
button_explore.grid(column=3,row=0)
#compute button for changing file qauntity File
button_Compute = tk.Button(window,
                        text = "Compute File Data",
                        command=lambda :[compute()] )
button_Compute.grid(column=3,row=1)
#CSV button for making all file to One single file
button_CSV = tk.Button(window,
                        text = "Create CSV Here",
                        command=lambda :[csv_making()] )
button_CSV.grid(column=3, row=2)
#Label for showing error During Different operation
label_for_error = tk.Label(window ,text="",
                            width=20,height=3)
label_for_error.grid(column=3,row=3)

#tkinter important making changes
window.mainloop()